# Plasma tiles wallpaper

A dynamic Plasma background that emulates the style of static Plasma wallpapers.

This wallpaper shows a surface consisting of colored hexagons with a few
pentagons.

The code for generating and rendering the surface is taken from [earthgen-old](https://github.com/vraid/earthgen-old).

![Screenshot](source/images/screenshot.png)

The rendering is done by [earthgen-old](https://github.com/vraid/earthgen-old).

## Build instructions

```bash
MY_SRC=/where/your/wallpaper/code/is
INSTALL_PREFIX=/where/your/wallpaper/goes
cd "$MY_SRC"
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX="$INSTALL_PREFIX" ..
make
make install
```

(`$INSTALL_PREFIX` is where you install your Plasma setup, replace it
accordingly)

To test the wallpaper, restart `plasmashell` with the location of the wallpaper:

```bash
kquitapp5 plasmashell
export XDG_DATA_DIRS=$INSTALL_PREFIX/share:$XDG_DATA_DIRS
export QML2_IMPORT_PATH=$INSTALL_PREFIX/lib/qml:$QML2_IMPORT_PATH
plasmashell
```

and then go to wallpaper settings and select it.

## Tutorials and resources

Plasma QML API explained
https://techbase.kde.org/Development/Tutorials/Plasma2/QML2/API
