/*
    SPDX-FileCopyrightText: %{CURRENT_YEAR} %{AUTHOR} <%{EMAIL}>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

import org.vandenoever.kde.plasma.tiles 1.0

Tiles {
    id: root
    waterPercentage: wallpaper.configuration.WaterPercentage
    gridSize: wallpaper.configuration.GridSize
}
