add_definitions(-DTRANSLATION_DOMAIN=\"plasma_wallpaper_org.vandenoever.kde.plasma.tiles\")

set(plasma_tiles_plugin_SRCS
    plasma_tiles_plugin.cpp
    earthgen-old/planet/grid/create_grid.cpp
    earthgen-old/planet/grid/grid.cpp
    earthgen-old/planet/grid/corner.cpp
    earthgen-old/planet/grid/edge.cpp
    earthgen-old/planet/grid/tile.cpp
    earthgen-old/planet/planet.cpp
    earthgen-old/planet/terrain/terrain_variables.cpp
    earthgen-old/planet/terrain/terrain_corner.cpp
    earthgen-old/planet/terrain/terrain_tile.cpp
    earthgen-old/planet/terrain/terrain.cpp
    earthgen-old/planet/terrain/terrain_generation.cpp
    earthgen-old/planet/terrain/terrain_edge.cpp
    earthgen-old/planet/terrain/river.cpp
    earthgen-old/planet/climate/climate_corner.cpp
    earthgen-old/planet/climate/season.cpp
    earthgen-old/planet/climate/climate_tile.cpp
    earthgen-old/planet/climate/climate_generation.cpp
    earthgen-old/planet/climate/climate_edge.cpp
    earthgen-old/planet/climate/climate.cpp
    earthgen-old/planet/climate/climate_variables.cpp
    earthgen-old/render/planet_renderer.cpp
    earthgen-old/render/globe_renderer.cpp
    earthgen-old/render/planet_colours.cpp
    earthgen-old/render/colour.cpp
    earthgen-old/math/vector3.cpp
    earthgen-old/math/vector2.cpp
    earthgen-old/math/matrix3.cpp
    earthgen-old/math/quaternion.cpp
    earthgen-old/math/matrix2.cpp
    earthgen-old/hash/md5.cpp
    earthgen-old/gui/planetHandler.cpp
)

add_library(plasma_tiles_plugin SHARED ${plasma_tiles_plugin_SRCS})

target_link_libraries(plasma_tiles_plugin
    KF5::I18n
    Qt5::Gui
    Qt5::Qml
    Qt5::Quick
    OpenGL::GL
)
install(TARGETS plasma_tiles_plugin DESTINATION ${KDE_INSTALL_QMLDIR}/org/vandenoever/kde/plasma/tiles)
install(FILES qmldir DESTINATION ${KDE_INSTALL_QMLDIR}/org/vandenoever/kde/plasma/tiles)
