#ifndef season_h
#define season_h

#include <vector>
#include "climate_tile.h"
#include "climate_corner.h"
#include "climate_edge.h"
class Planet;

class Season {
public:
	Season () {}
	
	std::vector<Climate_tile> tiles;
	std::vector<Climate_corner> corners;
	std::vector<Climate_edge> edges;
};

const std::vector<Climate_tile>& tiles (const Season&);
const std::vector<Climate_corner>& corners (const Season&);
const std::vector<Climate_edge>& edges (const Season&);

const Climate_tile& nth_tile (const Season&, int);
const Climate_corner& nth_corner (const Season&, int);
const Climate_edge& nth_edge (const Season&, int);

Climate_tile& m_tile (Season&, int);
Climate_corner& m_corner (Season&, int);
Climate_edge& m_edge (Season&, int);

#endif
