#include "edge.h"

Edge::Edge (int i) :
	id (i) {
	for (auto& t : tiles)
		t = nullptr;
	for (auto& c : corners)
		c = nullptr;
}

int position (const Edge& e, const Tile* t) {
	if (e.tiles[0] == t)
		return 0;
	else if (e.tiles[1] == t)
		return 1;
	return -1;
}
int position (const Edge& e, const Corner* c) {
	if (e.corners[0] == c)
		return 0;
	else if (e.corners[1] == c)
		return 1;
	return -1;
}

int sign (const Edge& e, const Tile* t) {
	if (e.tiles[0] == t)
		return 1;
	else if (e.tiles[1] == t)
		return -1;
	return 0;
}
int sign (const Edge& e, const Corner* c) {
	if (e.corners[0] == c)
		return 1;
	else if (e.corners[1] == c)
		return -1;
	return 0;
}

