#include "grid.h"
#include "create_grid.h"
#include "../planet.h"
#include <cmath>

int tile_count (int size) {return 10*pow(3,size)+2;}
int corner_count (int size) {return 20*pow(3,size);}
int edge_count (int size) {return 30*pow(3,size);}

Grid::Grid (int s) :
	size (s) {
	const int tile_count = ::tile_count(size);
	tiles.reserve(tile_count);
	for (int i=0; i<tile_count; i++)
		tiles.push_back(Tile(i, i<12 ? 5 : 6));
	const int corner_count = ::corner_count(size);
	corners.reserve(corner_count);
	for (int i=0; i<corner_count; i++)
		corners.push_back(Corner(i));
	const int edge_count = ::edge_count(size);
	edges.reserve(edge_count);
	for (int i=0; i<edge_count; i++)
		edges.push_back(Edge(i));
}

const std::vector<Tile>& tiles (const Planet& p) {return p.grid.tiles;}
const std::vector<Corner>& corners (const Planet& p) {return p.grid.corners;}
const std::vector<Edge>& edges (const Planet& p) {return p.grid.edges;}

const Tile* nth_tile (const Planet& p, int n) {return &p.grid.tiles[n];}
const Corner* nth_corner (const Planet& p, int n) {return &p.grid.corners[n];}
const Edge* nth_edge (const Planet& p, int n) {return &p.grid.edges[n];}

int tile_count (const Planet& p) {return p.grid.tiles.size();}
int corner_count (const Planet& p) {return p.grid.corners.size();}
int edge_count (const Planet& p) {return p.grid.edges.size();}

