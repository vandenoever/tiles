#ifndef grid_h
#define grid_h

#include <vector>
#include "tile.h"
#include "corner.h"
#include "edge.h"
class Planet;

class Grid {
public:
	Grid (int);
	
	int size;
	std::vector<Tile> tiles;
	std::vector<Corner> corners;
	std::vector<Edge> edges;
};

const std::vector<Tile>& tiles (const Planet&);
const std::vector<Corner>& corners (const Planet&);
const std::vector<Edge>& edges (const Planet&);

const Tile* nth_tile (const Planet&, int);
const Corner* nth_corner (const Planet&, int);
const Edge* nth_edge (const Planet&, int);

int tile_count (const Planet&);
int corner_count (const Planet&);
int edge_count (const Planet&);

#endif
