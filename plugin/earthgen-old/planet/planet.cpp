#include "planet.h"
#include "grid/grid.h"
#include "grid/create_grid.h"

Planet::Planet () :grid(size_n_grid(0)) {
}

Planet::~Planet () {
}

void set_grid_size (Planet& p, int size) {
	if (p.grid.size != size) {
		p.grid = size_n_grid(size);
	}
	p.terrain.clear_terrain();
	clear_climate(p);
}
