#ifndef planet_h
#define planet_h

#include "grid/grid.h"
#include "terrain/terrain.h"
#include "climate/climate.h"

class Planet {
public:
	Planet ();
	~Planet ();
	
	Grid grid;
	Terrain terrain;
	Climate climate;

	const Vector3& axis() const { return terrain.axis(); }
	double axial_tilt() const { return terrain.axial_tilt(); }
	double radius() const { return terrain.radius(); }
	double sea_level() const { return terrain.sea_level(); }
};

void set_grid_size (Planet&, int size);

#endif
