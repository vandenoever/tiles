#ifndef terrain_h
#define terrain_h

#include <vector>
#include "terrain_tile.h"
#include "terrain_corner.h"
#include "terrain_edge.h"
#include "terrain_variables.h"
#include "river.h"
class Planet;
class Tile;
class Quaternion;

class Terrain {
public:
	friend const std::vector<Terrain_tile>& tiles (const Terrain&);
	friend const std::vector<Terrain_corner>& corners (const Terrain&);
	Terrain () {}

	void clear_terrain();
	void init_terrain(const Planet&);

	const Terrain_tile& nth_tile (int n) const {return tiles[n];}
	const Terrain_corner& nth_corner (int n) const {return corners[n];}

	Terrain_tile& tile(int n) {return tiles[n];}
	Terrain_corner& corner(int n) {return corners[n];}
	Terrain_edge& edge(int n) {return edges[n];}

	const Vector3& axis() const { return var.axis; }
	void set_axis(const Vector3& axis) {
		var.axis = axis;
	}
	double axial_tilt() const { return var.axial_tilt; }
	void set_axial_tilt(double axial_tilt) {
		var.axial_tilt = axial_tilt;
	}
	double radius() const { return var.radius; }
	void set_radius(double radius) {
		var.radius = radius;
	}
	double sea_level() const { return var.sea_level; }
	void set_sea_level(double sea_level) {
		var.sea_level = sea_level;
	}
private:
	Terrain_variables var;
	std::vector<Terrain_tile> tiles;
	std::vector<Terrain_corner> corners;
	std::vector<Terrain_edge> edges;
};

double latitude (const Vector3&);
double longitude (const Vector3&);

double latitude (const Planet&, const Vector3&);
double longitude (const Planet&, const Vector3&);

// angle from corner 0 to north
double north (const Planet&, const Tile*);

double area (const Planet&, const Tile*);
double length (const Planet&, const Edge*);

double coriolis_coefficient (const Planet&, double);

Vector3 default_axis ();
Quaternion rotation (const Planet&);
// rotation to bring planet axis into default position
Quaternion rotation_to_default (const Planet&);

const Terrain& terrain (const Planet&);
Terrain& m_terrain (Planet&);

const std::vector<Terrain_tile>& tiles (const Terrain&);
const std::vector<Terrain_corner>& corners (const Terrain&);

#endif

