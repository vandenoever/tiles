#ifndef terrain_generation_h
#define terrain_generation_h

class Planet;
class Terrain_parameters;

void generate_terrain (Planet&, const Terrain_parameters&);

#endif
