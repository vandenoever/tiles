#ifndef terrain_tile_h
#define terrain_tile_h

#include "terrain_water.h"

class Terrain_tile {
public:
	Terrain_tile () :
		elevation (0), type (type_land) {}

	Terrain_water water;
	float elevation;
	int type;
	enum {type_land = 1, type_water = 2, type_coast = 4};

	bool is_land() const {return type & type_land;}
	bool is_water() const {return type & type_water;}
	bool has_coast() {return type & type_coast;}

	float water_depth() const {return water.depth;}
};


#endif
