#ifndef terrain_variables_h
#define terrain_variables_h

#include "../../math/vector3.h"

class Terrain_variables {
public:
	Terrain_variables () {}

	Vector3 axis;
	double axial_tilt;
	double radius;
	double sea_level;
};

#endif
