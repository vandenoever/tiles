/*
    SPDX-FileCopyrightText: %{CURRENT_YEAR} %{AUTHOR} <%{EMAIL}>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "plasma_tiles_plugin.h"
#include "earthgen-old/render/colour.h"
#include "earthgen-old/render/globe_renderer.h"
#include "earthgen-old/render/planet_colours.h"
#include "earthgen-old/gui/planetHandler.h"

// KF
#include <KLocalizedString>
// Qt
#include <QJSEngine>
#include <QQuickWindow>
#include <QtGui/QOpenGLShaderProgram>
#include <QSGGeometryNode>
#include <QSGSimpleRectNode>

void PlasmaTilesPlugin::registerTypes(const char* uri)
{
    Q_ASSERT(uri == QLatin1String("org.vandenoever.kde.plasma.tiles"));
    qmlRegisterType<Tiles>(uri, 1, 0, "Tiles");
}

struct PlanetData {
    PlanetHandler planetHandler;
    Planet_colours colours;
};

Tiles::Tiles() :m_program(nullptr), m_globeRenderer(new Globe_renderer)
{
    setFlag(QQuickItem::ItemHasContents, true);
    m_terrainParameters.seed = "smfgqcku";
    m_terrainParameters.iterations = 100;
    connect(this, &QQuickItem::windowChanged, this, &Tiles::handleWindowChanged);
}

void Tiles::setWaterPercentage(int value) {
    double new_value = value / 100.;
    if (m_terrainParameters.water_ratio != new_value) {
        m_terrainParameters.water_ratio = new_value;
        createNewPlanet();
    }
}

void Tiles::setGridSize(int value) {
    if (value > 8) {
        value = 8;
    }
    if (m_terrainParameters.grid_size != value) {
        m_terrainParameters.grid_size = value;
        createNewPlanet();
    }
}

void Tiles::createNewPlanet() {
    PlanetData* const d = new PlanetData();
    d->planetHandler.generateTerrain(m_terrainParameters);
    init_colours(d->colours, d->planetHandler.planet());
    set_colours(d->colours, d->planetHandler.planet(), 0);
    m_planet = QSharedPointer<const PlanetData>(d);
    update();
}

// This method is called on the rendering thread.
void Tiles::paint()
{
    // QOpenGLShaderProgram is only used, because without nothing is rendered.
    // It is not clear why QOpenGLShaderProgram is needed to get OpenGL
    // rendering to show anything.
    if (!m_program) {
        m_program = new QOpenGLShaderProgram();
        m_program->bindAttributeLocation("vertices", 0);
        m_program->link();
        connect(window()->openglContext(), SIGNAL(aboutToBeDestroyed()),
                this, SLOT(cleanup()), Qt::DirectConnection);
    }
    m_program->bind();
    m_program->enableAttributeArray(0);
    m_program->setAttributeArray(0, GL_FLOAT, nullptr, 0);
    m_program->disableAttributeArray(0);
    m_program->release();
    // end of QOpenGLShaderProgram workaround

    if (width() && height()) {
        glViewport(0, 0, width(), height());
        m_globeRenderer->set_viewport_size(width(), height());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0,0,0,0);
        double scale = std::max(width(), height()) / 400;
        m_globeRenderer->scale = scale;
        // take a copy so the planet stays valid while rendering
        const QSharedPointer<const PlanetData> planet = m_planet;
        if (planet) {
            const Planet* p = &planet->planetHandler.planet();
            m_globeRenderer->draw(*p, rotation_to_default(*p), planet->colours);
        }
    }
}

void Tiles::cleanup()
{
    delete m_program;
    m_program = nullptr;
}

void Tiles::handleWindowChanged(QQuickWindow *win)
{
    if (win) {
        // Connect the beforeRendering signal to our paint function.
        // Since this call is executed on the rendering thread it must be
        // a Qt::DirectConnection
        connect(win, SIGNAL(beforeRendering()), this, SLOT(paint()), Qt::DirectConnection);
        // If we allow QML to do the clearing, they would clear what we paint
        // and nothing would show.
        win->setClearBeforeRendering(false);
        win->setColor(Qt::black);
    }
}

// HACK!
// render a transparent node above the opengl to force it to update
QSGNode *Tiles::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
{
    QSGSimpleRectNode *n = static_cast<QSGSimpleRectNode *>(oldNode);
    if (!n) {
        n = new QSGSimpleRectNode();
        n->setColor(QColor(0,0,0,0));
    }
    n->setRect(boundingRect());
    return n;
}
