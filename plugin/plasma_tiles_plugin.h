/*
    SPDX-FileCopyrightText: %{CURRENT_YEAR} %{AUTHOR} <%{EMAIL}>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#ifndef PLASMA_TILESPLUGIN_H
#define PLASMA_TILESPLUGIN_H

#include <QQmlExtensionPlugin>
#include <QQuickItem>

#include "earthgen-old/planet/terrain/terrain_parameters.h"

class PlasmaTilesPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri) override;
};

class QOpenGLShaderProgram;
class Globe_renderer;
class PlanetData;

class Tiles : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(int waterPercentage READ waterPercentage WRITE setWaterPercentage)
    Q_PROPERTY(int gridSize READ gridSize WRITE setGridSize)
public:
    Tiles();
    int waterPercentage() const { return m_terrainParameters.water_ratio * 100; }
    void setWaterPercentage(int value);
    int gridSize() const { return m_terrainParameters.grid_size; }
    void setGridSize(int value);

public Q_SLOTS:
    void paint();
    void cleanup();

private Q_SLOTS:
    void handleWindowChanged(QQuickWindow *win);

protected:
    QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override;

private:
    void createNewPlanet();

    QOpenGLShaderProgram *m_program;
    QSharedPointer<Globe_renderer> m_globeRenderer;
    Terrain_parameters m_terrainParameters;
    QSharedPointer<const PlanetData> m_planet;
};
#endif // PLASMA_TILESPLUGIN_H
