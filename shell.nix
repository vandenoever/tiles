with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "plasma-env";
  buildInputs = [
    cmake
    extra-cmake-modules
    libsForQt5.ki18n
    libsForQt5.plasma-framework
    ninja
    qtcreator
    kdevelop
  ];
}
